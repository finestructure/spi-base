This project creates the base image to build the SPI Server image and push it to the Gitlab registry.

## Arm64 version

For local debugging on an Apple Silicon machine, build a local arm64 version of the base image:

```
cd /path/to/spi-base/checkout/
docker build -t registry.gitlab.com/finestructure/spi-base:0.14.1 .
```

By specifying the tag that's currently in use, `make docker-build` in the SPI Server project will now use the local arm64 version of the image for building (and subsequently testing).
