FROM swift:6.0.3-jammy

# install required tools
RUN apt-get update \
    && apt-get dist-upgrade -y \
    && apt-get install -y \
    ca-certificates tzdata \
    curl git make unzip \
    libjemalloc-dev \
    sqlite3 libsqlite3-dev  `# dependencies for building SPM` \
    libcurl4-openssl-dev    `# Foundation networking` \
    && rm -r /var/lib/apt/lists/*


# install swift toolchain so we can run `swift package dump-package`
# (enable this whenever we need to support an upcoming Swift version before updating the
# base image's swift version)
# WORKDIR /
# RUN mkdir swift-5.4 \
#     && cd swift-5.4 \
#     && curl https://swift.org/builds/swift-5.4-release/ubuntu1804/swift-5.4-RELEASE/swift-5.4-RELEASE-ubuntu18.04.tar.gz \
#     | tar -xvz --strip-components 1
